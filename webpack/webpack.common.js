const htmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require("vue-loader");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin/lib');
const webpack = require('webpack');
const { root, isdev } = require('./helper');
function commonConfig() {
  return {
    entry: {
      main: root('src', 'main.ts')
    },
    output: {
      assetModuleFilename: 'images/[hash][ext][query]',
      clean: true,
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)$/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                appendTsSuffixTo: [/\.vue$/],
              }
            }
          ],
          include: root('src'),
          exclude: [/node_modules/]
        },
        {
          test: /\.(js|jsx)$/,
          use: 'babel-loader',
          include: root('src'),
          exclude: [/node_modules/]
        },
        {
          test: /\.vue$/,
          use: [
            {
              loader: 'vue-loader',
              options: {
                loaders: {
                  js: 'awesome-typescript-loader'
                }
              }
            }
          ],
          include: root('src'),
          exclude: [/node_modules/]

        },
        {
          test: /\.(css|sass|scss)$/,
          use: [
            'style-loader',
            isdev ? "vue-style-loader" : {
              loader: MiniCssExtractPlugin.loader,
              options: {
                esModule: false,
              },
            },
            'css-loader',
            'sass-loader'],
          include: root('src'),
          exclude: [/node_modules/]
        },
        {
          test: /\.(eot|ttf|woff|woff2)(\?\S*)?$/,
          type: 'asset/resource',
          generator: {
            filename: 'assets/[hash][ext][query]'
          },
        },
        {
          test: /\.(png|jpe?g|gif|webm|mp4|svg)$/,
          type: 'asset/resource',
          generator: {
            filename: 'assets/[hash][ext][query]'
          },
        },
      ]
    },
    plugins: [
      new htmlWebpackPlugin({
        template: root('public/index.html'),
        favicon: root('public/favicon.ico'),
        appMountId: 'app',
        publicPath: '/'
      }),
      new VueLoaderPlugin(),
      new CleanWebpackPlugin({
        dry: true,
        verbose: true,
        cleanStaleWebpackAssets: false,
        cleanOnceBeforeBuildPatterns: [root('dist', '**', '*')]
      }),
      new ForkTsCheckerWebpackPlugin(),
    ],
    resolve: {
      alias: {
        'vue$': isdev ? 'vue/dist/vue.esm.js' : 'vue/dist/vue.runtime.min.js',
        '@': root('src')
      },
      extensions: ['*', '.js', '.vue', '.json', '.ts']
    },
    optimization: {
      runtimeChunk: "single",
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: "vendors",
            priority: -10,
            chunks: "all",
          },
        },
      },
    },
    devtool: "source-map"
  }
}

module.exports = commonConfig;