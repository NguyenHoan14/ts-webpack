'use strict';
const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const { root, getFreePort } = require('./helper');
const { getJSDocReturnTag } = require('typescript');
// const port = 8080;
async function devConfig() {
  const port = await getFreePort(8080);
  return {
    mode: process.env.NODE_ENV,
    devtool: 'eval-source-map',
    entry: {
      hmr: [
        // cái này giúp refresh sau compile
        "webpack-dev-server/client?http://localhost:" + port,
        // cái này tác động đến hoạt động refesh khi compile success
        "webpack/hot/only-dev-server",
      ],
    },
    output: {
      path: root('dist'),
      filename: 'js/[name].bundle.js',
      chunkFilename: 'js/[id].chunk.js',
    },
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all'
      }
    },
    plugins: [
      new FriendlyErrorsPlugin(),
      new webpack.HotModuleReplacementPlugin(),
    ],
    devtool: "source-map",
    devServer: {
      open: true,
      historyApiFallback: true,
      hot: true,
      port: port,
      watchOptions: {
        poll: true
      },
      contentBase: root('dist'),
      host: 'localhost',
      noInfo: true,
      stats: {
        colors: true,
        hash: false,
        version: false,
        timings: false,
        chunks: false,
        modules: false,
        reasons: false,
        children: false,
        source: true,
        errors: true,
        errorDetails: true,
        warnings: true
      },
      after: function (app, server, compiler) {
        console.log('App is running at: http://localhost:'+port)
      },
    }
  }
}

module.exports = devConfig;