'use strict';
const net = require('net');
const isInUse = (port) => {
    return new Promise((resolve, reject) => {
        let client = net.connect(port, 'localhost');
        client.once('connect', () => {
            client.end()
            resolve(true)
        }
        );
        client.once('error', () => {
            client.end()
            resolve(false);
        })
    });
};

exports.getFreePort = async (port) => {
    while (true) {
        const inUse = await isInUse(port);
        if (inUse) port += 1;
        if (!inUse) return port;
    }
}
const path = require('path');

const _root = path.resolve(__dirname, '..');

exports.root = function (args) {
    args = Array.prototype.slice.call(arguments, 0);

    return path.join.apply(path, [_root].concat(args));
};

exports.assetsPath = function (_path) {
    return path.posix.join('static', _path);
};

exports.isdev = process.env.NODE_ENV === 'development';
