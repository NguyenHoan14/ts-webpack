import { lazyRoute, LazyRoute } from "@/utils";
const router: LazyRoute[] = [
  lazyRoute("/", "about", "about/views/index"),
  lazyRoute("/2/", "about2", "about/views/x")
];
export default router;
