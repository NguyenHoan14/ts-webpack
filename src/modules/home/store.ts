const state = {
  username: 'name',
  userList: []
};
const getters = {};
const actions = {
  queryUserLists({commit}: any, thing: any){
    commit('setUserLists', thing)
  }
};
const mutations = {
  setUserLists(state: any, payload: any){
    state.userList = payload
  }
};
export const moduleName = 'home'
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
