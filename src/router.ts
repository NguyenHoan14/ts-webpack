import Vue from "vue";
import Router, { RouterOptions } from "vue-router";
import { lazyRoute, LazyRoute } from "@/utils";
Vue.use(Router);
function loadRoutes() {
  const context = require.context("@/modules", true, /router.ts$/i);
  const keys: string[] = context.keys();
  const values:LazyRoute[][] = keys.map(context).map((e: any)=>e.default);
  console.log(values)
  return keys.reduce((acc: LazyRoute[][], key: string, i) =>{
    const base = key.split('/')[1];
    const route = values[i].map((e: LazyRoute)=>{
      return {...e, path: '/'+base+ e.path}
    })
    return [...acc, route]
  }, [])
}
console.log(loadRoutes())
let routes: LazyRoute[] = [
  {
    path: "/",
    redirect: "/home",
  },
];
const appendRoutes = loadRoutes().reduce(
  (arr: LazyRoute[], value: LazyRoute[]) => {
    return arr.concat(value);
  },
  []
);
routes = [...routes, ...appendRoutes];
console.log(routes)
const router = new Router({
  base: '/',
  mode: "history",
  routes,
});
export default router;
