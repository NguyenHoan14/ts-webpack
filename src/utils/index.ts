interface Meta {
  requireAuth: boolean;
}
export interface LazyRoute {
  path: string;
  name?: string;
  component?: any;
  meta?: Meta;
  redirect?: string;
}
export function lazyRoute(
  path: string,
  name: string,
  view: string,
  requireAuth = true
): LazyRoute {
  console.log(`@/modules/${view}.vue`)
  return {
    path,
    name,
    component: () => import(`@/modules/${view}.vue`),
    meta: { requireAuth },
  };
}
