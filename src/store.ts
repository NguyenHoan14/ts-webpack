import Vue from "vue";
import Vuex from "vuex";

function loadModule() {
  const context = require.context("@/modules", true, /store.ts$/i);
  return context
    .keys()
    .map(context)
    .reduce((obj: { [key: string]: any }, current: any) => {
      const { moduleName, default: module } = current;
      console.log('-------------',moduleName, module)

      if (moduleName===undefined || moduleName === null ) return obj;
      return { ...obj, [moduleName]: module };
    }, {} as { [key: string]: any });
}
Vue.use(Vuex);


export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: loadModule(),
});
