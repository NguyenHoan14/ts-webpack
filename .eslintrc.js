module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
    "@vue/prettier/@typescript-eslint",
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/no-unused-vars": "off",
    quotemark: ["off", "single"],
    indent: ["off", "spaces", 2],
    "interface-name": "off",
    "ordered-imports": "off",
    "object-literal-sort-keys": "off",
    "no-consecutive-blank-lines": "off",
    "no-empty": "off",
    curly: ["off", "ignore-same-line"],
    "max-classes-per-file": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "max-len": ["error", { code: 200 }],
    "@typescript-eslint/ban-types": [
      "error",
      {
        types: {
          String: false,
          Boolean: false,
          Number: false,
          Symbol: false,
          "{}": false,
          Object: false,
          object: false,
          Function: false,
        },
        extendDefaults: true,
      },
    ],
  },
};
