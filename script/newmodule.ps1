param ($module)
$codeDir = "$pwd\src\modules"
if (($null -eq $module) -or ($module -eq "")) {
    $module = read-host -Prompt "Please enter a module's name"
    if (($null -eq $module) -or ($module -eq "")) {
        throw "module's name was not given, command exit!"
    }
}
$codeDir += "\$module"
New-Item -Path "$codeDir" -ItemType Directory
New-Item -Path "$codeDir\router.ts" -ItemType File
New-Item -Path "$codeDir\store.ts" -ItemType File
New-Item -Path "$codeDir\views" -ItemType Directory
New-Item -Path "$codeDir\views\index.vue" -ItemType File
New-Item -Path "$codeDir\components" -ItemType Directory
New-Item -Path "$codeDir\components\component.vue" -ItemType File
New-Item -Path "$codeDir\styles" -ItemType Directory

# write default content:
Add-Content -Path "$codeDir\router.ts" -Value "
import { lazyRoute, LazyRoute } from '@/utils';
const router: LazyRoute[] = [
  lazyRoute('/index', 'index', '$module/views/index')
];
export default router;
"
############################################################################

Add-Content -Path "$codeDir\views\index.vue" -Value '
<template>
  <div>
  </div>
</template>

<script lang="ts">
import { Vue, Component } from "vue-property-decorator";
@Component
export default class ClassName extends Vue {

}
</script>
<style lang="scss" scoped>

</style>
'

##########################################################

Add-Content -Path "$codeDir\store.ts" -Value '
const state = {};
const getters = {};
const actions = {};
const mutations = {};
// module name is required and must be change value
// do not use duplicate mudule name in project
export const moduleName = "someModuleName"
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
'

Add-Content -Path "$codeDir\components\component.vue" -Value '
<template>
  <div>
  </div>
</template>

<script lang="ts">
import { Vue, Component } from "vue-property-decorator";
@Component
export default class ClassName extends Vue {

}
</script>
<style lang="scss" scoped>

</style>
'
