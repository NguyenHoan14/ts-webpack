'use strict';
const commonConfig = require('./webpack/webpack.common');
const devConfig = require('./webpack/webpack.dev');
const proConfig = require('./webpack/webpack.pro');
const {isdev} = require('./webpack/helper');
const {merge} = require('webpack-merge')
async function config(){
  let config = await devConfig();
  if(!isdev) config = proConfig();
  const common= commonConfig();
  return merge(common, config)
}

module.exports = config;